package com.example.pairprogramming;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	Button btn;
	TextView tv;
	Integer status=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		findView();
		tv.setTextSize(20);
		btnListener();
	}

	

	private void btnListener() {
		// TODO Auto-generated method stub
		btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(status % 2== 1){
					tv.setText("onClick");
					tv.setTextColor(Color.BLUE);					
				}					
				else{
					tv.setText("waitClick");
					tv.setTextColor(0xffff0000);
				}					
				status++;
			}
			
		});
	}



	private void findView() {
		// TODO Auto-generated method stub
		btn = (Button)findViewById(R.id.btn);
		tv = (TextView)findViewById(R.id.tv);
	}	

}
